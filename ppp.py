import websocket
import json,threading
try:
    import thread
except ImportError:
    import _thread as thread
import time

from flask import Flask,render_template
from flask_socketio import SocketIO

app = Flask(__name__)
lists=[]

socketio=SocketIO(app, async_mode='threading')
app.debug=False

app.use_reloader=False

def on_message(ws, message):
    temp = json.loads(message)
    if temp['subscription'] == 'orders':
        data = {}
        data['country_code'] = temp['message']['shipping']['country_code']
        lists.append(data)
        # print(data)

def watching():
    while True:
        time.sleep(0.1)
        if lists:
            chunk=lists.pop()
            socketio.emit('hello', chunk,namespace='/client')


def on_error(ws, error):
    print(error)

def on_close(ws):
    print("### closed ###")

def on_open(ws):
    def run(*args):
        while True:
            time.sleep(100)
    thread.start_new_thread(run, ())


@app.route('/')
def hello_1():
    return render_template('Ray_test2.html')


if __name__ == "__main__":

    websocket.enableTrace(True)
    ws = websocket.WebSocketApp("wss://stable-incubator.thehut.net/cashboard-websocket-orders/?filter=orders",
                              on_message = on_message,
                              on_error = on_error,
                              on_close = on_close)
    wst=threading.Thread(target=ws.run_forever)
    wst.daemon = True
    wst.start()


    socketio.start_background_task(watching)
    socketio.run(app)
