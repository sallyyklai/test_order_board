import websocket,json,threading,time,jinja2
try:
    import thread
except ImportError:
    import _thread as thread
from flask import Flask,render_template
from flask_socketio import SocketIO

app = Flask(__name__)
lists=[]

socketio=SocketIO(app, async_mode='threading')
app.debug=False

app.use_reloader=False

def on_message(ws, message):
    temp = json.loads(message)
    if temp['subscription'] == 'orders':
        data = {}
        data['currency'] = temp['message']['total_items_price']['currency']
        data['value'] = temp['message']['total_items_price']['value']
        data['gbp_value'] = temp['message']['total_items_price']['gbp_value']
        data['country_code'] = temp['message']['shipping']['country_code']
        data['channel'] = temp['message']['property']['channel']
        lists.append(data)
        # print(data)

def watching():
    # lists_copy_for_pop = list.copy()
    accumulate_value = [{'country_code': 'GB', 'value': 636953.2387832357}, {'country_code': 'CN', 'value': 636953.2387832357}, {'country_code': 'CN', 'value': 636953.2387832357}, {'country_code': 'CN', 'value': 636953.2387832357}, {'country_code': 'CN', 'value': 636953.2387832357}, {'country_code': 'CN', 'value': 636953.2387832357}, {'country_code': 'CN', 'value': 636953.2387832357}, {'country_code': 'CN', 'value': 636953.2387832357}, {'country_code': 'CN', 'value': 636953.2387832357}, {'country_code': 'CN', 'value': 636953.2387832357}, {'country_code': 'CN', 'value': 636953.2387832357}, {'country_code': 'CN', 'value': 636953.2387832357}, {'country_code': 'CN', 'value': 636953.2387832357}, {'country_code': 'CN', 'value': 636953.2387832357}, {'country_code': 'CN', 'value': 636953.2387832357}, {'country_code': 'CN', 'value': 636953.2387832357}, {'country_code': 'CN', 'value': 636953.2387832357}, {'country_code': 'CN', 'value': 636953.2387832357}]
    accumulate_order = [{'country_code': 'GB', 'total_order_number':3000}]

    socketio.emit('my_response',{'accumulate_value':accumulate_value,'accumulate_order':accumulate_order},namespace='/client')
    while True:
        time.sleep(0.2)
        if lists:
            chunk = lists.pop()
        # if len(lists) >= 10:
        #     chunk = lists
            # print(len(lists))
            socketio.emit('my_response', {'channel':chunk['channel'], 'count': round(float(chunk['gbp_value']), 2), 'code':chunk['country_code']  },namespace='/client')
            # lists.clear()

def on_error(ws, error):
    print(error)

def on_close(ws):
    print("### closed ###")

def on_open(ws):
    def run(*args):
        while True:
            time.sleep(100)
    thread.start_new_thread(run, ())

@app.route('/')
def hello_1():
    return render_template('bike.html')

# @socketio.on('connect', namespace='/client')
# def test_connect():
#     global thread
#     with thread_lock:
#         if thread is None:
#             thread = socketio.start_background_task(target=background_thread)


if __name__ == "__main__":
    websocket.enableTrace(True)
    ws = websocket.WebSocketApp("wss://stable-incubator.thehut.net/cashboard-websocket-orders/?filter=orders",
                              on_message = on_message,
                              on_error = on_error,
                              on_close = on_close)
    wst = threading.Thread(target=ws.run_forever)
    wst.daemon = True
    wst.start()

    socketio.start_background_task(watching)
    socketio.run(app)

    app.run(debug=True)
