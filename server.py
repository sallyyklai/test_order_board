#In terminal, $export FLASK_APP=server.py $export FLASK_ENV=development
from flask import Flask,render_template
app = Flask(__name__)

class order:
    def __init__(self,country,value):
        self.country = country
        self.value = value

orders = [{"currency":"USD","value":22.99,"gbp_value":17.6749419,"country_code":"US","channel":"zavvi"},{"currency":"USD","value":141.75,"gbp_value":108.97881749999999,"country_code":"US","channel":"glossybox"},{"currency":"EUR","value":129.11,"gbp_value":112.4675626434014,"country_code":"DE","channel":"myprotein"},{"currency":"GBP","value":23.44,"gbp_value":23.44,"country_code":"GB","channel":"myprotein"},{"currency":"USD","value":163.8,"gbp_value":125.931078,"country_code":"US","channel":"skinstore"},{"currency":"GBP","value":30.08,"gbp_value":30.08,"country_code":"GB","channel":"myprotein"},{"currency":"EUR","value":41.21,"gbp_value":35.897980454918844,"country_code":"FR","channel":"mankind"},{"currency":"GBP","value":94.25,"gbp_value":94.25,"country_code":"TW","channel":"beautyexpert"},{"currency":"EUR","value":22.74,"gbp_value":19.808786108829278,"country_code":"IT","channel":"myprotein"},{"currency":"GBP","value":21.98,"gbp_value":21.98,"country_code":"GB","channel":"myprotein"}]

@app.route('/')
def hello():
    return render_template('heatmap.html',orders=orders)

if __name__ == '__main__':
    app.run(debug=True)
