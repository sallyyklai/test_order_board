* heat1.html is now running with cummulating order value
* heat2.html is now running with cummulating order counter
* heat3.html is now running with interchanging value-couter heatmap
  * flash between switching between each other
  * no flashing when update value within same type of graph (for value heatmap, as I let it exist on page longer than counter so that it can update value before being switched by counter heatmap)
* bike.html is now interchanging two heat maps and the marker maps
  * with sudo data from bigquery running as initialisation
