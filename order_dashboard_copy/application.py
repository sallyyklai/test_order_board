#!/usr/bin/env python
from threading import Lock
from flask import Flask, render_template, session, request
from flask_socketio import SocketIO, emit, disconnect
import websocket
import json
# from google.cloud import bigquery

try:
    import thread
except ImportError:
    import _thread as thread
import time



def background_thread():
    def on_message(ws, message):
        temp = json.loads(message)
        if temp['subscription'] == 'orders':
            data = {}
            data['currency'] = temp['message']['total_items_price']['currency']
            data['value'] = temp['message']['total_items_price']['value']
            data['gbp_value'] = temp['message']['total_items_price']['gbp_value']
            data['country_code'] = temp['message']['shipping']['country_code']
            data['channel'] = temp['message']['property']['channel']
            socketio.sleep(0.01)
            socketio.emit('my_response',
                          {'count': round(float(data['gbp_value']), 2), 'code':data['country_code'], 'channel':data['channel']},
                          namespace='/client')


    def on_error(ws, error):
        print(error)

    def on_close(ws):
        print("### closed ###")

    def on_open(ws):
        def run(*args):
            while True:
                time.sleep(100)
        thread.start_new_thread(run, ())


    websocket.enableTrace(True)
    ws = websocket.WebSocketApp("wss://stable-incubator.thehut.net/cashboard-websocket-orders/?filter=orders",
                                  on_message = on_message,
                                  on_error = on_error,
                                  on_close = on_close)
    ws.on_open = on_open
    ws.run_forever()

# Set this variable to "threading", "eventlet" or "gevent" to test the
# different async modes, or leave it set to None for the application to choose
# the best option based on installed packages.
# Personally, I install and use eventlet
async_mode = None
app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, async_mode=async_mode)
thread = None
thread_lock = Lock()

# Route for serving up the index page
@app.route('/')
def index():
    return render_template('bike.html', async_mode=socketio.async_mode)
    # _with_list_and_database

# This function is called when a web browser connects
@socketio.on('connect', namespace='/client')
def test_connect():
    global thread
    with thread_lock:
        if thread is None:
            thread = socketio.start_background_task(target=background_thread)

    # bigquery_client = bigquery.Client('the-hut-group')
    # QUERY=("SELECT shipping.country_code , SUM(total_items_price.gbp_value) as total_value_by_country \
    # FROM `the-hut-group.orders.site_orders20181112` \
    # group by shipping.country_code \
    # LIMIT 1000")
    # query_job=bigquery_client.query(QUERY)
    # dataList=[]
    # rows=query_job.result()
    # for row in rows:
    #     dataList.append({'country_code':row.country_code, 'value':row.total_value_by_country})



    accumulate_value = [{'country_code': 'GB', 'value': 636953.2387832357}, {'country_code': 'CN', 'value': 636953.2387832357}, {'country_code': 'CN', 'value': 636953.2387832357}, {'country_code': 'CN', 'value': 636953.2387832357}, {'country_code': 'CN', 'value': 636953.2387832357}, {'country_code': 'CN', 'value': 636953.2387832357}, {'country_code': 'CN', 'value': 636953.2387832357}, {'country_code': 'CN', 'value': 636953.2387832357}, {'country_code': 'CN', 'value': 636953.2387832357}, {'country_code': 'CN', 'value': 636953.2387832357}, {'country_code': 'CN', 'value': 636953.2387832357}, {'country_code': 'CN', 'value': 636953.2387832357}, {'country_code': 'CN', 'value': 636953.2387832357}, {'country_code': 'CN', 'value': 636953.2387832357}, {'country_code': 'CN', 'value': 636953.2387832357}, {'country_code': 'CN', 'value': 636953.2387832357}, {'country_code': 'CN', 'value': 636953.2387832357}, {'country_code': 'CN', 'value': 636953.2387832357}]\

    accumulate_order = [{'country_code': 'GB', 'total_order_number': 600}, {'country_code': 'CN', 'total_order_number': 100}]

    emit('my_response',{'accumulate_value': accumulate_value, 'accumulate_order': accumulate_order })

# Ping-pong allows Javascript in the web page to calculate the
# connection latency, averaging over time
@socketio.on('my_ping', namespace='/client')
def ping_pong():
    emit('my_pong')

# Notification that a client has disconnected
@socketio.on('disconnect', namespace='/client')
def test_disconnect():
    print('Client disconnected', request.sid)

# Run the web app
if __name__ == '__main__':
    socketio.run(app, debug=True, host='0.0.0.0')
